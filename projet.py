#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 11 22:57:01 2021

@author: matylde
"""

import numpy as np


def read_interaction_file_dict(filename):
    """
    Reads a protein-protein interaction graph from filename and stocks\
        interactions into dictionnary.
    
    Creates sublist for each binary interaction.

    Parameters
    ----------
    filename : TSV-file
        First line = total number of interactions
        Rest = binary interactions

    Returns
    -------
    Dictionnary where each key is a protein and the value associated is the\
        neighbour's list.

    """
    # Reading file
    fd = open(filename, 'r')
    lines = fd.readlines()
    fd.close()
    inter = [line.split() for line in lines] # Split each interaction
    inter = inter[1:len(inter)] # Remove first line
    dico = {}
    for line in inter: # For each interaction
        for i in range(0,len(line)):
            if line[i] not in dico: # If the protein isn't a key in dico
                dico[line[i]] = {} # Add protein as key in dico 
    for key in dico.keys(): # For each key
        ipp = []
        for line in inter: # For each interaction in file
            for j in range(0,len(line)): # For each protein in interaction
                if line[j] is key: # If the protein corresponds to key
                    for k in range(0,len(line)):
                        if line[k] is not key: 
                            ipp.append(line[k]) # Add the associated protein
        dico[key] = ipp # Associate all interactive protein to key
    return dico


def read_interaction_file_list(filename):
    """
    Reads a protein-protein interaction graph from filename and stocks\
        interactions into list.
    
    Creates sublist for each binary interaction.

    Parameters
    ----------
    filename : TSV-file
        First line = total number of interactions
        Rest = binary interactions

    Returns
    -------
    List of all sublists created.

    """
    # Reading file
    fd = open(filename, 'r')
    lines = fd.readlines()
    fd.close()
    inter = [line.split() for line in lines] # Split each interaction
    inter = inter[1:len(inter)] # Remove first line
    ipp = []
    for line in inter:
        ipp.append(line) # Add each interaction to list
    return ipp


def read_interaction_file_mat(filename):
    """
    Reads a protein-protein interaction graph from filename and stocks\
        interactions into adjacency matrix and ordered protein names into list.
        
    Adds each protein into an ordered list of edges/proteins.
    Fills interaction matrix according to the order of previously created list.


    Parameters
    ----------
    filename : TSV-file
        First line = total number of interactions
        Rest = binary interactions

    Returns
    -------
    Tuple: ordered protein names list and adjacency interaction matrix.

    """
    with open(filename, "r") as file:
        ipp = file.readlines()[1:] # removes file's first line = number of interactions
        ipp_mat = np.zeros((len(ipp), len(ipp))) # creates matrix of dim len(ipp)*len(ipp)
        edges_list = []
        for line in ipp:
            data = line.split() # removes last character = \n and specifies sep = \t
            for prot in data:
                if prot not in edges_list:
                    edges_list.append(prot) # appends each protein/edge into edges_list
            for i in range(0, len(edges_list)):
                if data[0] == edges_list[i]: # checks if first protein from file matches i^th protein from edges_list
                    for j in range(0, len(edges_list)):
                        if data[1] == edges_list[j]: # checks if second protein from file matches j^th protein from edges_list
                            ipp_mat[i][j] = ipp_mat[j][i] = 1 
        return({'list_V':edges_list, 'matrix':ipp_mat}) # returns dictionary


def read_interaction_file(filename):
    """
    Reads a protein-protein interaction graph from filename and stocks\
        interactions into dictionnary, list, adjacency matrix and ordered\
        protein names list.
        
    Uses previously created functions.

    Parameters
    ----------
    filename : TSV-file
        First line = total number of interactions
        Rest = binary interactions

    Returns
    -------
    Tuple: interaction dictionnary, interaction list, interaction matrix\
        and list of protein names.

    """
    d_int = read_interaction_file_dict(filename)
    l_int = read_interaction_file_list(filename)
    m_int = read_interaction_file_mat(filename)['matrix']
    l_som = read_interaction_file_mat(filename)['list_V']
    return (d_int, l_int, m_int, l_som)


def is_interaction_file(filename):
    """
    Reads a protein-protein interaction graph from filename and check\
        if it corresponds to the requiered format : first line with\
        total number of interactions, only binary interactions, not\
        an empty file.
        
    Parameters
    ----------
    filename : TSV-file
        First line = total number of interactions
        Rest = binary interactions

    Returns
    -------
    Boolean: True if respect the file format. 
        False if not respected.

    """
    # Reading file
    fd = open(filename, 'r')
    lines = fd.readlines()
    fd.close()

    # Check for empty file
    if lines == []:
        return False
    inter = [line.split() for line in lines]

    # Check that first line equals to the amount of interactions
    first_l = [int(i) for i in inter[0]]
    if first_l != [len(inter)-1]:
        return False

    # Check for the amount of interactions
    inter = inter[1:len(inter)] # Remove first line
    for i in range(0,len(inter)):
        if len(inter[i]) != 2:
            return False      
    return True
