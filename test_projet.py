import unittest
from projet import *

class Test_IntP(unittest.TestCase):
    def test_read_interaction_file_dict_is_dict(self):
        dico = read_interaction_file_dict("toy_example.txt")
        self.assertEqual(type(dico), dict)

    def test_read_interaction_file_list_is_list(self):
        ls = read_interaction_file_list("toy_example.txt")
        self.assertEqual(type(ls), list)

    def test_read_interaction_file_mat_is_dict(self):
        dico = read_interaction_file_mat("toy_example.txt")
        self.assertEqual(type(dico), dict)

    def test_read_interaction_file_dict(self):
        dico = read_interaction_file_dict("toy_example.txt")
        self.assertEqual(dico, {'A': ['B', 'C'], 'B': ['A', 'C', 'D'], 'C': ['A', 'B'], 'D': ['B', 'E', 'F'], 'E': ['D'], 'F': ['D']})

    def test_read_interaction_file_list(self):
        ls = read_interaction_file_list("toy_example.txt")
        self.assertEqual(ls, [['A', 'B'], ['A', 'C'], ['B', 'C'], ['B', 'D'], ['D', 'E'], ['D', 'F']])


    # def test_read_interaction_file_mat_mat(self):
    #     mat = read_interaction_file_mat("toy_example.txt")['matrix']
    #     mat_ref = np.array([[0, 1, 1, 0, 0, 0], [1, 0, 1, 1, 0, 0], [1, 1, 0, 0, 0, 0], [0, 1, 0, 0, 1, 1], [0, 0, 0, 1, 0, 0], [0, 0, 0, 1, 0, 0]])
    #     self.assertEqual(mat, mat_ref)


    def test_read_interaction_file_mat_list(self):
        list_V = read_interaction_file_mat("toy_example.txt")['list_V']
        self.assertEqual(list_V, ['A', 'B', 'C', 'D', 'E', 'F'])


    # def test_read_interaction_file(self):
    #     tup = read_interaction_file("toy_example.txt")
    #     self.assertEqual(tup, ({'A': ['B', 'C'], 'B': ['A', 'C', 'D'], 'C': ['A', 'B'], 'D': ['B', 'E', 'F'], 'E': ['D'], 'F': ['D']}, [['A', 'B'], ['A', 'C'], ['B', 'C'], ['B', 'D'], ['D', 'E'], ['D', 'F']], array([[0., 1., 1., 0., 0., 0.],
    #    [1., 0., 1., 1., 0., 0.],
    #    [1., 1., 0., 0., 0., 0.],
    #    [0., 1., 0., 0., 1., 1.],
    #    [0., 0., 0., 1., 0., 0.],
    #    [0., 0., 0., 1., 0., 0.]]), ['A', 'B', 'C', 'D', 'E', 'F']))
    

    def test_is_interaction_file(self):
        test = is_interaction_file("toy_example.txt")
        self.assertEqual(test, True)


unittest.main()
